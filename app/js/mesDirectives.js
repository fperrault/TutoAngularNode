angular.module('mesDirectives', [])

.directive("contenuMail", function(){
    return {
        restrict: "E",//nouvel attribut html
        template: '<div class="spacer"> \
                        <div class="well"> \
                            <h1> {{email.subject}} </h1> \
                            <p><label>De :</label> {{email.from}} </p> \
                            <p><label>&Agrave; :</label> {{email.to}} </p> \
                            <p><label>Date :</label> {{email.date | date:"short"}} </p> \
                            <p ng-bind-html="email.content"></p> \
                        </div> \
                    </div>',
        scope: {
            email: "="
        }
    }
})

.directive("nouveauMail", function(){
    return {
        restrict: "E",
        template: '<div class="spacer"> \
                    <form id="formNouveauMail" name="formNouveauMail"> \
                        <div class="input-group"> \
                            <span class="input-group-addon labelChampNouveauMail">&Agrave</span> \
                            <input type="text" class="form-control" ng-model="nouveauMail.to" > \
                        </div> \
                        <div class="input-group"> \
                            <span class="input-group-addon laberChampNouveauMail">Object</span> \
                            <input type="text" class="form-control" ng-model="nouveauMail.subject"> \
                        </div> \
                        <div class="spacer"> \
                            <textarea ui-tinymce="optionsTinyMce" class="contenuNouveauMail" rows="10" ng-model="nouveauMail.content"></textarea> \
                        </div> \
                        <div class="spacer text-center"> \
                            <button ng-click="clicEnvoiMail()" class="btn btn-success">Envoyer <span class="hSpacer glyphicon glyphicon-send"></span></button> \
                            <button class="btn btn-danger" ng-click="eraseMail()" ng-disabled="formNouveauMail.$pristine"><!--Bouton desactivé si rien saisi-->Effacer <span class="hSpacer glyphicon glyphicon-remove"></button> \
                        </div> \
                    </form> \
                </div>',
        scope: {
            envoiMail: "&"
        },
        controller: function($scope){

            $scope.optionsTinyMce = {
                language: "fr_FR",
                statusbar: false,
                menubar: false
            }

            $scope.eraseMail = function(){
                $scope.nouveauMail = {
                    from: "François",
                    date: new Date()
                }
                if(tinyMCE.activeEditor){
                    tinyMCE.activeEditor.setContent("")
                }
                $scope.formNouveauMail.$setPristine() //reset du modele
            }

            $scope.clicEnvoiMail = function(){
                const regExpValid = new RegExp("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,6}$", "gi")
                
                if(!$scope.nouveauMail.to || !$scope.nouveauMail.to.match(regExpValid)){//si adresse vide OU non valide
                    alert("Adresse mail non valide")
                    return
                }
        
                if(!$scope.nouveauMail.subject){
                    var confirmMail = window.confirm("Voulez vous envoyer un mail sans objet?")
                    if(!confirmMail){
                        return
                    }
                }

                $scope.envoiMail({ nouveauMail: $scope.nouveauMail });
            }

            $scope.$on("initFormNewMail", function(){//addeventListener pour passer la fonction a la vue
                $scope.eraseMail()
            })
        }
    }
})