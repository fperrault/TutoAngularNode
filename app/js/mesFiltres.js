angular.module('mesFiltres', [])

.filter("surbrillanceRecherche", function(){
	return function(input, recherche){
		if(recherche){	
			return input.replace(new RegExp("(" + recherche + ")" , "gi"), "<span class='surbrillanceRecherche'>$1</span>") //regexp g=toute la chaine, i=on ignore la casse
		} else {
			return input
		}
	}
})