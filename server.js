//Modules internes
var http = require("http")
var fs = require("fs")

//Modules externes
var mime = require("mime")//test les "Content-type"
var express = require("express")//serveur
var bodyParser = require("body-parser")

var serviceMails = require(__dirname +  "/getMails.js")

    //MIDDLEWARE
    var serveStatic = require("serve-static")//gestion des fichiers static en fonction de l'url
    var logger = require("morgan")//générer des log
    var favicon = require("serve-favicon")//gère favicon sur toutes les pages


var PORT = 8080

serviceMails.genererMails()


var serveurConnect = express()

serveurConnect.use(favicon(__dirname + "/app/doc/favicon.ico"))
serveurConnect.use(logger(":method :url"))//dans console : les methodes et les url
serveurConnect.use(serveStatic(__dirname + "/app", {index : "index.html"}))//donne le chemin des fichiers à charger



//-----------------------------------------
//API REST
//-----------------------------------------

var api = express()

//Récupérer la liste des dossiers
api.get("/dossiers", serviceMails.getDossiers)

//Récupérer un dossier
api.get("/dossiers/:idDossier", serviceMails.getDossier)

//récupérer un mail
api.get("/dossiers/:idDossier/:idMail", serviceMails.getMail)

//Envoyer un mail
api.use(bodyParser.json())

api.post("/envoi", serviceMails.envoiMail)


serveurConnect.use("/api", api)


http.createServer(serveurConnect).listen(PORT)
console.log("Serveur démarré sur le port :" + PORT)