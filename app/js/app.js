var app = angular.module("webmail", ['ngSanitize', 'ui.tinymce', 'mailServiceMock', 'mesFiltres', 'mesDirectives'])//dépendances vers modules

app.controller("webmailCtrl", function($scope, $location, $filter, mailService){//services



	//---------------------------------------------
	//Gestion du tri
	//---------------------------------------------
	$scope.champTri = null
	$scope.triDescendant = false

	$scope.mailOrderBy = function(champDeTri){
		if($scope.champTri == champDeTri){
			$scope.triDescendant = !$scope.triDescendant
		} else {
			$scope.champTri = champDeTri
			$scope.triDescendant = false
		}
	}

	$scope.styleChevron = function(champDeTri){
		return { 
			glyphicon: $scope.champTri == champDeTri,
			'glyphicon-chevron-up': $scope.champTri == champDeTri && !$scope.triDescendant, //objet JS = entre'' si caractères spéciaux
			'glyphicon-chevron-down': $scope.champTri == champDeTri && $scope.triDescendant 
		}
	}

	//---------------------------------------------
	//Recherche
	//---------------------------------------------
	$scope.recherche = null
	$scope.dropTheCache = function(){//methode pour supprimer le texte de la barre de recherche
		$scope.recherche = null
	}


	//---------------------------------------------
	//Creation Mail
	//---------------------------------------------
	$scope.afficherNouveauMail = false
	

	$scope.envoiMail = function(nouveauMail){

		mailService.envoiMail(nouveauMail)
		$location.path("/")

	}



	//---------------------------------------------
	//NAVIGATION
	//---------------------------------------------

	$scope.vueCourante = null
	$scope.selectedFolder = null
	$scope.selectedMail = null
	

    $scope.selectFolder = function(dossier){
		$scope.vueCourante = 'vueDossier'
		$scope.selectedFolder = mailService.getDossier(dossier)
	}
	
	$scope.selectMail = function(valueDossier, idMail){
		$scope.selectedMail = mailService.getMail(valueDossier, idMail)
	}

	$scope.versEmail = function(dossier, mail){
		$scope.vueCourante = 'vueContenuMail'
		$location.path("/" + dossier.value + "/" + mail.id)
	}

	$scope.$watch(function(){
		return $location.path()
	}, function(newPath){
		var tabPath = newPath.split("/")//créer un tab & un élément = entre ce caractère "/" donc tab[0] = " "
		if(tabPath.length > 1 && tabPath[1]){
			if(tabPath[1] == "nouveauMail"){//Si click nouveauMail, alors on initialise
				$scope.vueCourante = 'vueNouveauMail'
				$scope.$broadcast("initFormNewMail")//lance l'evenement
			} else {
				var valueDossier = tabPath[1]
				if(tabPath.length > 2){
					var idMail = tabPath[2]
					$scope.selectMail(valueDossier, idMail)
				} else {
					$scope.selectFolder(valueDossier)
				}
			}
		} else {
			$scope.vueCourante = null
		}
	})

	$scope.dossiers = mailService.getDossiers()
})